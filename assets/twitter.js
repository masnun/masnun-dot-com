jQuery(function($){
    $("#my-tweets").tweet({
        avatar_size: 32,
        count: 5,
        fetch: 20,
        username: ["masnun"],
        filter: function(t){ return ! /^@\w+/.test(t.tweet_raw_text); },
        loading_text: "Loading tweets",
        template: "{avatar} {text}"
    });
});
